import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Charity } from '../../dto/charity';
import { ParticipationDTO } from "../../dto/ParticipationDTO";
import { DonationDTO } from "../../dto/DonationDTO";
import { CharityProgress } from "../../dto/CharityProgress";
import { CharityService } from '../../services/charity.service';
import { AppService } from '../../services/app.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-charity-detail',
  templateUrl: './charity-detail.component.html',
  styleUrls: ['./charity-detail.component.css']
})
export class CharityDetailComponent implements OnInit {

  charity: Charity;
  charityProgress: CharityProgress;
  isDonateModalVisible: boolean;
  isParticipantModalVisible: boolean;
  donationDTO = new DonationDTO();
  partipationDTO = new ParticipationDTO;

  constructor(
    private route: ActivatedRoute,
    private charityService: CharityService,
    private appService: AppService,
    private router: Router,
    private toastService: ToastService
  ) { }

  ngOnInit(): void {
    this.isDonateModalVisible = false;
    this.getCharityById();
  }

  getCharityById() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.charityService.getCharityById(id).subscribe(charity => {
      if (charity) {
        this.charity = charity
        this.getCharityProgress();
      }
    });
  }

  private getCharityProgress() {
    this.charityService.getCharityProgress(this.charity.id).subscribe(charityProgress => {
      if (charityProgress) {
        console.log(charityProgress);
        this.charityProgress = charityProgress;
      }
    })
  }

  donate() {
    this.donationDTO.charity = this.charity.name;
    this.donationDTO.username = sessionStorage.getItem("username");

    this.charityService.donateToCharity(this.donationDTO).subscribe(data => {
      if (data) {
        this.toastService.showSuccess('You have made a donation!', 'Congratulations');
        this.getCharityProgress();
      }
    })

    this.isDonateModalVisible = false;
    this.donationDTO.donation = 0;
  }

  participate() {
    this.partipationDTO.username = sessionStorage.getItem("username");
    this.partipationDTO.charity = this.charity.name;

    this.charityService.participateInCharity(this.partipationDTO).subscribe(data => {
      if (data) {
        this.toastService.showSuccess('You are a volunteer!', 'Congratulations');
        this.getCharityProgress();
      }
    })

    this.isParticipantModalVisible = false;
  }

  openDonationModal() {
    this.isAuthenticated();
    this.isDonateModalVisible = true;
  }

  openParticipantModal() {
    this.isAuthenticated();
    this.isParticipantModalVisible = true;
  }

  private isAuthenticated() {
    if (!this.appService.isAuthenticated()) {
      this.router.navigate(['login']);
    }
  }
  closeDonationModal() {
    this.isDonateModalVisible = false;
    this.donationDTO.donation = 0;
  }

  closeParticipantModal() {
    this.isParticipantModalVisible = false;
  }

  calcMaxDonation() {
    return this.charity.totalBudget - this.charityProgress.donations;
  }


  needsParticipant() {
    return this.charity.numberOfParticipants != this.charityProgress.participants;
  }

  deleteCharity() {
    console.log("deleting")
    this.charityService.deleteCharity(this.charity.id).subscribe();
    this.router.navigate(['main']);
  }

  canDelete() {
    if (this.charity && this.appService.isAuthenticated()) {
      return this.charity.creator === sessionStorage.getItem("username");
    }

    return false;
  }

}
