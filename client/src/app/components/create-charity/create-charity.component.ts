import { Component, OnInit } from '@angular/core';
import { CharityService } from '../../services/charity.service';
import { Charity } from '../../dto/charity';
import { AuthenticationService } from '../../services/authentication.service';
import { ToastService } from 'src/app/services/toast.service';
import { Router } from '@angular/router';

@Component({
  templateUrl: './create-charity.component.html',
  styleUrls: ['./create-charity.component.css']
})
export class CreateCharityComponent implements OnInit {

  constructor(
    private charityService: CharityService,
    private toastService: ToastService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  charity = new Charity();

  submit() {
    console.log(this.charity);
    this.charity.id = undefined;
    this.charity.creator = sessionStorage.getItem('username');

    this.charityService.addCharity(this.charity).subscribe(newCharity => {
      if (newCharity) {
        if (newCharity.name === this.charity.name) {
          this.toastService.showSuccess('You added a charity', "Congratulations!")
          this.router.navigate(['/main']);
        }
      }
    });
  }

  onFileSelected(event) {
    if (event.target.files.length > 0) {
      console.log(event.target.files[0].name);
      this.charity.image = event.target.files[0].name;
    }
  }
}
