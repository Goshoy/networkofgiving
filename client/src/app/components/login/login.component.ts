import { Component, OnInit } from '@angular/core';
import { Credentials } from '../../dto/credentials';
import { AuthenticationService } from '../../services/authentication.service';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials = new Credentials();

  constructor(
    private authenticationService: AuthenticationService,
    private appService: AppService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  login() {
    this.authenticationService.authenticate(this.credentials.username, this.credentials.password).subscribe(data => {
      console.log(data);
      if (data.token) {
        let tokenStr = 'Bearer ' + data.token;
        this.appService.authenticate(this.credentials.username, tokenStr);

        let previousUrl = this.route.snapshot.paramMap.get('previousUrl');
        if (previousUrl === '/register' || previousUrl == null) {
          this.router.navigate(['/main']);
        } else {
          this.router.navigate([previousUrl]);
        }
      }
    });


  }
}

