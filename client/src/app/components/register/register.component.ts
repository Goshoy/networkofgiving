import { Component, OnInit } from '@angular/core';
import { UserDTO } from '../../dto/user';
import { CountryService } from '../../services/country.service';
import { Country } from '../../dto/country';
import { UsersService } from '../../services/users.service';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  countries: Country[];
  user = new UserDTO();

  constructor(
    private countryService: CountryService,
    private userService: UsersService,
    private router: Router,
    private toastService: ToastService
  ) { }

  ngOnInit(): void {
    this.getCountries();
  }

  getCountries() {
    this.countryService.getCountries().subscribe(data => {
      this.countries = data
      console.log(this.countries);
    });
  }

  submit() {
    console.log(this.user);
    this.userService.registerUser(this.user).subscribe(data => {
      if (data) {
        this.toastService.showSuccess("User has been registered successfully!", "");
        let url = '/register';
        this.router.navigate(['/login', { 'previousUrl': url }]);
      }
    });
  }

}
