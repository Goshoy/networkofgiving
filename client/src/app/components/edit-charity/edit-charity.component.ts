import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CharityService } from '../../services/charity.service';
import { AppService } from '../../services/app.service';
import { Charity } from '../../dto/charity';

@Component({
  selector: 'app-edit-charity',
  templateUrl: './edit-charity.component.html',
  styleUrls: ['./edit-charity.component.css']
})
export class EditCharityComponent implements OnInit {

  charity: Charity;

  constructor(
    private route: ActivatedRoute,
    private charityService: CharityService
  ) { }

  ngOnInit(): void {
    this.getCharityById();
  }

  getCharityById() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.charityService.getCharityById(id).subscribe(charity => {
      if (charity) {
        this.charity = charity
      }
    });
  }

  updateCharity() {
    this.charityService.updateCharity(this.charity.id, this.charity).subscribe(data => {
      console.log(data);
      if(data) {

      }
    })
  }

}
