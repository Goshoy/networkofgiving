import { Component, OnInit } from '@angular/core';

import { Charity } from '../../dto/charity';
import { CharityService } from '../../services/charity.service';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  charities: Charity[];

  constructor(
    private charityService: CharityService,
    private appService: AppService
  ) { }

  ngOnInit(): void {
    this.getAllCharities();
  }

  getAllCharities() {
    this.charityService.getCharities().subscribe(charities => {
      if (charities) {
        console.log(charities);
        this.charities = charities
      }
    });
  }

  search(event) {
    const searchString = event.target.value;
    if (!searchString) {
      this.getAllCharities();
    } else {
      this.charityService.searchCharities(searchString).subscribe(charities => {
        if (charities) {
          console.log(charities);
          this.charities = charities
        }
      });
    }
  }

  isAuthenticated() {
    return this.appService.isAuthenticated();
  }
}
