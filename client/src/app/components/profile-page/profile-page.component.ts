import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { UserDTO } from '../../dto/user';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {

  user: UserDTO;
  constructor(private userService: UsersService) { }

  ngOnInit(): void {
    let username = sessionStorage.getItem('username');
    this.userService.getUser(username).subscribe(data => {
      if (data) {
        this.user = data;
      }
    })
  }

}
