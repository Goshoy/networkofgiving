import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private authenticated: boolean;

  constructor() {
    this.authenticated = false;
  }

  logout() {
    this.authenticated = false;
    sessionStorage.removeItem('username');
    // sessionStorage.setItem('token', null);
  }

  isAuthenticated() {
    return this.authenticated;
  }

  authenticate(username: string, tokenStr: string) {
    sessionStorage.setItem('username', username);
    sessionStorage.setItem('token', tokenStr);
    this.authenticated = true;
  }

}
