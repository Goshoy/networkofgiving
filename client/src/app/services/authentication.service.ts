import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HandleErrorService } from './handle-error.service';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient, private errorService: HandleErrorService) { }

  authenticate(username, password): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}/authenticate`, { username, password }).pipe(
      tap(_ => console.log("authenticated user")),
      catchError(this.errorService.handleError<any[]>('authenticate', []))
    );
  }

}