import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Country } from '../dto/country';
import { catchError, tap } from 'rxjs/operators';
import { HandleErrorService } from './handle-error.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  private countriesUrl = `${environment.apiUrl}/countries`;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient, private errorService: HandleErrorService) { }

  getCountries(): Observable<Country[]> {
    return this.http.get<Country[]>(this.countriesUrl + "/all").pipe(
      tap(_ => console.log("fetched charities")),
      catchError(this.errorService.handleError<Country[]>('getCountries', []))
    );
  }
}
