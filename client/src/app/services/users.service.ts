import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserDTO } from '../dto/user';
import { catchError, tap } from 'rxjs/operators';
import { HandleErrorService } from './handle-error.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private usersUrl = `${environment.apiUrl}/users`;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient, private errorService: HandleErrorService) { }

  registerUser(user: UserDTO): Observable<UserDTO> {
    return this.http.post<UserDTO>(this.usersUrl + "/register", user, this.httpOptions).pipe(
      tap(_ => console.log("fetched registered user")),
      catchError(this.errorService.handleError<UserDTO>('registerUser'))
    );
  }

  getUser(username: string): Observable<UserDTO> {
    return this.http.get<UserDTO>(this.usersUrl + "/" + username, this.httpOptions).pipe(
      tap(_ => console.log("fetched user profile")),
      catchError(this.errorService.handleError<UserDTO>('getUser'))
    );
  }

}
