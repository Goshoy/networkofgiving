import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Charity } from '../dto/charity';
import { ParticipationDTO } from "../dto/ParticipationDTO";
import { DonationDTO } from "../dto/DonationDTO";
import { CharityProgress } from "../dto/CharityProgress";
import { catchError, tap } from 'rxjs/operators';
import { HandleErrorService } from './handle-error.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CharityService {
  private charitiesUrl = `${environment.apiUrl}/charities`;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient, private errorService: HandleErrorService) { }

  getCharities(): Observable<Charity[]> {
    return this.http.get<Charity[]>(this.charitiesUrl + "/all").pipe(
      tap(_ => console.log("fetched charities")),
      catchError(this.errorService.handleError<Charity[]>('getCharities', []))
    );
  }

  searchCharities(expr: string): Observable<Charity[]> {
    return this.http.get<Charity[]>(this.charitiesUrl + "/search/" + expr).pipe(
      tap(_ => console.log("fetched matching charities")),
      catchError(this.errorService.handleError<Charity[]>('searchCharities', []))
    );
  }

  getCharityById(id: number): Observable<Charity> {
    return this.http.get<Charity>(this.charitiesUrl + "/" + id).pipe(
      tap(_ => console.log(`fetched charity id=${id}`)),
      catchError(this.errorService.handleError<Charity>(`getCharity id=${id}`))
    );
  }

  getCharityProgress(id: number): Observable<CharityProgress> {
    return this.http.get<CharityProgress>(this.charitiesUrl + "/progress/" + id).pipe(
      tap(_ => console.log(`fetched charity progress id=${id}`)),
      catchError(this.errorService.handleError<CharityProgress>(`getCharityProgress id=${id}`))
    );
  }

  addCharity(charity: Charity): Observable<Charity> {
    return this.http.post<Charity>(this.charitiesUrl + "/add", charity, this.httpOptions).pipe(
      tap((newCharity: Charity) => console.log(`added charity w/ id=${newCharity.id}`)),
      catchError(this.errorService.handleError<Charity>('addCharity'))
    );
  }

  donateToCharity(donation: DonationDTO): Observable<DonationDTO> {
    return this.http.post<DonationDTO>(this.charitiesUrl + "/donate", donation, this.httpOptions).pipe(
      tap(_ => console.log(`made donation`)),
      catchError(this.errorService.handleError<DonationDTO>('donateToCharity'))
    );
  }

  participateInCharity(participation: ParticipationDTO): Observable<ParticipationDTO> {
    return this.http.post<ParticipationDTO>(this.charitiesUrl + "/participate", participation, this.httpOptions).pipe(
      tap(_ => console.log(`became a participant`)),
      catchError(this.errorService.handleError<ParticipationDTO>('participateInCharity'))
    );
  }

  deleteCharity(id: number): Observable<any> {
    return this.http.delete(this.charitiesUrl + "/delete/" + id, this.httpOptions).pipe(
      tap(_ => console.log(`deleted a charity`)),
      catchError(this.errorService.handleError<any>('deleteCharity'))
    );
  }

  updateCharity(id: number, charity: Charity): Observable<any> {
    return this.http.patch(this.charitiesUrl + "/update/" + id, charity, this.httpOptions).pipe(
      tap(_ => console.log(`updated a charity`)),
      catchError(this.errorService.handleError<any>('updateCharity'))
    );
  }


}
