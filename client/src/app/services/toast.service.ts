import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
})
export class ToastService {

    constructor(private toastr: ToastrService) { }

    showError(message: string, header: string) {
        this.toastr.error(message, header);
    }


    showInfo(message: string, header: string) {
        this.toastr.info(message, header);
    }


    showSuccess(message: string, header: string) {
        this.toastr.success(message, header);
    }

}
