import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class HandleErrorService {

  constructor(private toastService: ToastService) { }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      if (error.error) {
        this.toastService.showError(error.error.message, "");
        console.error(error.error.message); // log to console instead
      } else {
        // TODO: better job of transforming error for user consumption
        console.log(`${operation} failed: ${error.message}`);
      }

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

