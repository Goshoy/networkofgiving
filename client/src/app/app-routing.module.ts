import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { LoginComponent } from './components/login/login.component';
import { CharityDetailComponent } from './components/charity-detail/charity-detail.component';
import { CreateCharityComponent } from './components/create-charity/create-charity.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './guards/auth.guard';
import { ProfilePageComponent } from './components/profile-page/profile-page.component';
import { EditCharityComponent } from './components/edit-charity/edit-charity.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';


const routes: Routes = [
  { path: '', redirectTo: '/main', pathMatch: 'full' },
  { path: 'main', component: MainComponent },
  { path: 'createCharity', component: CreateCharityComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'charity/:id', component: CharityDetailComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfilePageComponent },
  { path: 'editCharity/:id', component: EditCharityComponent, canActivate: [AuthGuard] },

  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
