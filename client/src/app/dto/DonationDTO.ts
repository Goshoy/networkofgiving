export class DonationDTO {
  donation: number;
  username: string;
  charity: string;
}
