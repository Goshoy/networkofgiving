export class UserDTO {
    firstName: string;
    lastName: string;
    username: string;
    age: number;
    gender: string;
    country: string;
    password: string;
}