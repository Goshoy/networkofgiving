export class Charity {
  id: number;
  name: string;
  description: string;
  image: string;
  numberOfParticipants: number;
  totalBudget: number;
  creator: string;
}

