import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from './services/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ui-screens';
  isLogOutModalVisible: boolean;

  constructor(
    private appService: AppService,
    private router: Router,
  ) { 
    this.isLogOutModalVisible = false;
  }

  logout() {
    this.appService.logout();
    this.closeLogoutModal();
    this.router.navigate(['main']);
  }

  isAuthenticated() {
    return this.appService.isAuthenticated();
  }

  closeLogoutModal() {
    this.isLogOutModalVisible = false;
  }

  showLogOutModal() {
    this.isLogOutModalVisible = true;
  }

}
