import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { Charity } from '../charity';
let CreateCharityComponent = class CreateCharityComponent {
    constructor(charityService) {
        this.charityService = charityService;
        this.charity = new Charity();
    }
    ngOnInit() {
    }
    submit() {
        console.log(this.charity);
        this.charity.id = undefined;
        this.charity.creator = "Minchata6";
        this.charityService.addCharity(this.charity).subscribe(newCharity => {
            if (newCharity.name === this.charity.name) {
                alert('Charity added successfully');
            }
        });
    }
};
CreateCharityComponent = __decorate([
    Component({
        selector: 'app-create-charity',
        templateUrl: './create-charity.component.html',
        styleUrls: ['./create-charity.component.css']
    })
], CreateCharityComponent);
export { CreateCharityComponent };
//# sourceMappingURL=create-charity.component.js.map