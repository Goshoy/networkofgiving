import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
let CharityService = class CharityService {
    constructor(http, errorService) {
        this.http = http;
        this.errorService = errorService;
        this.charitiesUrl = 'http://localhost:8080/charities';
        this.httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        };
    }
    getCharities() {
        return this.http.get(this.charitiesUrl + "/all").pipe(tap(_ => console.log("fetched heroes")), catchError(this.errorService.handleError('getCharities', [])));
    }
    getCharityById(id) {
        return this.http.get(this.charitiesUrl + "/" + id).pipe(tap(_ => console.log(`fetched charity id=${id}`)), catchError(this.errorService.handleError(`getCharity id=${id}`)));
    }
    addCharity(charity) {
        console.log("here");
        return this.http.post(this.charitiesUrl + "/add", charity, this.httpOptions).pipe(tap((newCharity) => console.log(`added charity w/ id=${newCharity.id}`)), catchError(this.errorService.handleError('addCharity')));
        ;
    }
};
CharityService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], CharityService);
export { CharityService };
//# sourceMappingURL=charity.service.js.map