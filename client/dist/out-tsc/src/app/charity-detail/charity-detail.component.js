import { __decorate } from "tslib";
import { Component } from '@angular/core';
let CharityDetailComponent = class CharityDetailComponent {
    constructor(route, charityService) {
        this.route = route;
        this.charityService = charityService;
    }
    ngOnInit() {
        this.getCharityById();
    }
    getCharityById() {
        const id = +this.route.snapshot.paramMap.get('id');
        this.charityService.getCharityById(id).subscribe(charity => this.charity = charity);
    }
};
CharityDetailComponent = __decorate([
    Component({
        selector: 'app-charity-detail',
        templateUrl: './charity-detail.component.html',
        styleUrls: ['./charity-detail.component.css']
    })
], CharityDetailComponent);
export { CharityDetailComponent };
//# sourceMappingURL=charity-detail.component.js.map