import { __decorate } from "tslib";
import { Component } from '@angular/core';
let MainComponent = class MainComponent {
    constructor(charityService) {
        this.charityService = charityService;
    }
    ngOnInit() {
        this.getAllCharities();
    }
    getAllCharities() {
        this.charityService.getCharities().subscribe(charities => {
            if (charities) {
                this.charities = charities;
            }
        });
    }
};
MainComponent = __decorate([
    Component({
        selector: 'app-main',
        templateUrl: './main.component.html',
        styleUrls: ['./main.component.css']
    })
], MainComponent);
export { MainComponent };
//# sourceMappingURL=main.component.js.map