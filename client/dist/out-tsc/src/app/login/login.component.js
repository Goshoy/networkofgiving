import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { Credentials } from '../credentials';
let LoginComponent = class LoginComponent {
    constructor(userService) {
        this.userService = userService;
        this.credentials = new Credentials();
    }
    ngOnInit() {
    }
    login() {
        this.userService.login(this.credentials).subscribe(data => console.log(data));
    }
};
LoginComponent = __decorate([
    Component({
        selector: 'app-login',
        templateUrl: './login.component.html',
        styleUrls: ['./login.component.css']
    })
], LoginComponent);
export { LoginComponent };
//# sourceMappingURL=login.component.js.map