import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
let CountryService = class CountryService {
    constructor(http, errorService) {
        this.http = http;
        this.errorService = errorService;
        this.countriesUrl = 'http://localhost:8080/countries';
        this.httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        };
    }
    getCountries() {
        return this.http.get(this.countriesUrl + "/all").pipe(tap(_ => console.log("fetched charities")), catchError(this.errorService.handleError('getCountries', [])));
    }
};
CountryService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], CountryService);
export { CountryService };
//# sourceMappingURL=country.service.js.map