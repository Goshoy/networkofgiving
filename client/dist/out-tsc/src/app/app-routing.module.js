import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { CharityDetailComponent } from './charity-detail/charity-detail.component';
import { CreateCharityComponent } from './create-charity/create-charity.component';
import { RegisterComponent } from './register/register.component';
const routes = [
    { path: 'main', component: MainComponent },
    { path: 'createCharity', component: CreateCharityComponent },
    { path: 'login', component: LoginComponent },
    { path: 'charity/:id', component: CharityDetailComponent },
    { path: 'register', component: RegisterComponent },
    { path: '', redirectTo: '/main', pathMatch: 'full' },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forRoot(routes)],
        exports: [RouterModule]
    })
], AppRoutingModule);
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map