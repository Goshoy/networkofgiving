import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
let UsersService = class UsersService {
    constructor(http, errorService) {
        this.http = http;
        this.errorService = errorService;
        this.usersUrl = 'http://localhost:8080/users';
        this.httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        };
    }
    registerUser(user) {
        return this.http.post(this.usersUrl + "/register", user, this.httpOptions).pipe(tap(_ => console.log("fetched registered user")), catchError(this.errorService.handleError('registerUser')));
    }
    login(credentials) {
        return this.http.post(this.usersUrl + "/login", credentials, this.httpOptions).pipe(tap(_ => console.log("fetched logged in user")), catchError(this.errorService.handleError('loginUser')));
    }
};
UsersService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], UsersService);
export { UsersService };
//# sourceMappingURL=users.service.js.map