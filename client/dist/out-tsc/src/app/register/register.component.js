import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { UserDTO } from '../user';
let RegisterComponent = class RegisterComponent {
    constructor(countryService, userService, router) {
        this.countryService = countryService;
        this.userService = userService;
        this.router = router;
        this.user = new UserDTO();
    }
    ngOnInit() {
        this.getCountries();
    }
    getCountries() {
        this.countryService.getCountries().subscribe(data => {
            this.countries = data;
            console.log(this.countries);
        });
    }
    submit() {
        console.log(this.user);
        this.userService.registerUser(this.user).subscribe(data => {
            if (data) {
                alert("User has been registered successfully!");
                this.router.navigate(['login']);
            }
        });
    }
};
RegisterComponent = __decorate([
    Component({
        selector: 'app-register',
        templateUrl: './register.component.html',
        styleUrls: ['./register.component.css']
    })
], RegisterComponent);
export { RegisterComponent };
//# sourceMappingURL=register.component.js.map