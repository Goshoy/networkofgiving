import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
let HandleErrorService = class HandleErrorService {
    constructor() { }
    handleError(operation = 'operation', result) {
        return (error) => {
            // TODO: send the error to remote logging infrastructure
            console.error(error.error.message); // log to console instead
            alert(error.error.message);
            // TODO: better job of transforming error for user consumption
            //this.log(`${operation} failed: ${error.message}`);
            // Let the app keep running by returning an empty result.
            return of(result);
        };
    }
};
HandleErrorService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], HandleErrorService);
export { HandleErrorService };
//# sourceMappingURL=handle-error.service.js.map