import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
let AppService = class AppService {
    constructor() {
        this.authenticated = false;
    }
    login() {
        this.authenticated = true;
    }
    logout() {
        this.authenticated = false;
    }
    isAuthenticated() {
        return this.authenticated;
    }
};
AppService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], AppService);
export { AppService };
//# sourceMappingURL=app.service.js.map