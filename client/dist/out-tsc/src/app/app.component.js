import { __decorate } from "tslib";
import { Component } from '@angular/core';
let AppComponent = class AppComponent {
    constructor(appService) {
        this.appService = appService;
        this.title = 'ui-screens';
    }
    login() {
        this.appService.login();
    }
    logout() {
        this.appService.logout();
    }
    isAuthenticated() {
        return this.appService.isAuthenticated();
    }
};
AppComponent = __decorate([
    Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.css']
    })
], AppComponent);
export { AppComponent };
//# sourceMappingURL=app.component.js.map