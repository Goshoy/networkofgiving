create schema charity;
use charity;

CREATE TABLE countries 
    ( country_id      INT NOT NULL AUTO_INCREMENT PRIMARY KEY       
    , country_name    VARCHAR(40) unique
    ); 

CREATE TABLE locations
    ( location_id    INT NOT NULL AUTO_INCREMENT PRIMARY KEY
    , street_address VARCHAR(40)
    , city       	 VARCHAR(30)        
    , country_id     INT
    , CONSTRAINT fk_country_id FOREIGN KEY (country_id) REFERENCES countries(country_id)
    ) ;
  
CREATE TABLE users
    ( user_id    	 INT NOT NULL AUTO_INCREMENT PRIMARY KEY
    , first_name     VARCHAR(20) NOT NULL
    , last_name      VARCHAR(25) NOT NULL
    , username		 VARCHAR(25) NOT NULL UNIQUE
    , age			 INT NOT NULL
    , gender		 VARCHAR(6)
    , location_id	 INT
    , CONSTRAINT     gender_check
					 CHECK (gender IN('Male', 'Female', 'Other'))
	, CONSTRAINT fk_location_id FOREIGN KEY (location_id) REFERENCES locations(location_id)
    ) ;

CREATE TABLE charities
	( charity_id     			INT NOT NULL AUTO_INCREMENT PRIMARY KEY
	, name		 				VARCHAR(100) NOT NULL UNIQUE
    , description    			VARCHAR(1000) NOT NULL
    , image 					VARCHAR(100) 
    , number_of_participants	INT 
    , total_budget				INT
    , owner_id 					INT NOT NULL
    , CONSTRAINT				participants_budget_requirement
								CHECK (number_of_participants IS NOT NULL OR total_budget IS NOT NULL)
	, CONSTRAINT fk_owner_id FOREIGN KEY (owner_id) REFERENCES users(user_id)
    ) ;
    
    insert into charities(name, description, number_of_participants, total_budget, owner_id) values('New1', 'New1 new', 3, 2000, 1);
    
CREATE TABLE logins
	( login_id		 INT NOT NULL AUTO_INCREMENT PRIMARY KEY
	, username		 VARCHAR(25) NOT NULL UNIQUE
    , password		 VARCHAR(512) NOT NULL
    , user_id		 INT NOT NULL
    , CONSTRAINT 	 fk_user_id FOREIGN KEY (user_id) REFERENCES users(user_id)
);
    
CREATE TABLE donations
	( donation_id	INT NOT NULL AUTO_INCREMENT PRIMARY KEY
    , amount		INT NOT NULL
    , charity_id	INT NOT NULL
    , donator_id	int NOT NULL
    , date			DATETIME
    , CONSTRAINT	fk_charity_id FOREIGN KEY (charity_id) REFERENCES charities(charity_id)
    , CONSTRAINT	fk_donator_id FOREIGN KEY (donator_id) REFERENCES users(user_id));

CREATE TABLE participations
	( participation_id	INT NOT NULL AUTO_INCREMENT PRIMARY KEY
    , charity_id		INT NOT NULL
    , participant_id	int NOT NULL
    , date				DATETIME
    , CONSTRAINT		fk_participations_charity_id FOREIGN KEY (charity_id) REFERENCES charities(charity_id)
    , CONSTRAINT		fk_participator_id FOREIGN KEY (participant_id) REFERENCES users(user_id));

insert into countries(country_name) values('Bulgaria');

insert into countries(country_name) values('Germany');