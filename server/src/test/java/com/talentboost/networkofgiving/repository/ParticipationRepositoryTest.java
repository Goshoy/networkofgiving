package com.talentboost.networkofgiving.repository;

import com.talentboost.networkofgiving.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class ParticipationRepositoryTest {
    @Autowired
    private ICountryRepository countryRepository;
    @Autowired
    private ILocationRepository locationRepository;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private ICharityRepository charityRepository;
    @Autowired
    private IParticipationRepository participationRepository;

    @BeforeEach
    public void setUp() {
        this.countryRepository.save(new Country("Bulgaria"));
        this.locationRepository.save(
                new Location(null, null, this.countryRepository.findByCountryName("Bulgaria")));

        Long country_id = this.countryRepository.findByCountryName("Bulgaria").getCountry_id();
        User user = new User("Georgi", "Ivanov", "gIvanov", 22, "Male",
                this.locationRepository.getLocationByStreeCityAndCountryID(null, null, country_id));
        User user1 = new User("Georgi", "Ivanov", "gIvanov1", 22, "Male",
                this.locationRepository.getLocationByStreeCityAndCountryID(null, null, country_id));
        this.userRepository.save(user);
        this.userRepository.save(user1);

        Charity charity = new Charity("charity1", "new charity", null,
                5, 2000, user);
        Charity charity1 = new Charity("charity2", "new charity", null,
                5, 2000, user1);
        this.charityRepository.save(charity);
        this.charityRepository.save(charity1);

        Participation newParticipation = new Participation(user, charity, null);
        this.participationRepository.save(newParticipation);
    }

    @Test
    public void testAddedParticipationIsActuallyAdded() {
        List<Participation> participations = this.participationRepository.findAll();
        assertThat(participations).hasSize(1);
    }

    @Test
    public void testMultipleParticipationsToSameCharityAreAdded() {
        User user1 = this.userRepository.findByUsername("gIvanov1");
        Charity charity = this.charityRepository.findByName("charity1");
        Participation newParticipation1 = new Participation(user1, charity, null);

        this.participationRepository.save(newParticipation1);

        List<Participation> participations = this.participationRepository.findAll();
        assertThat(participations).hasSize(2);
    }

    @Test
    public void testMultipleParticipationsToDifferentCharitiesAreAdded() {
        User user1 = this.userRepository.findByUsername("gIvanov1");
        Charity charity1 = this.charityRepository.findByName("charity2");
        Participation newParticipation1 = new Participation(user1, charity1, null);

        this.participationRepository.save(newParticipation1);

        List<Participation> participations = this.participationRepository.findAll();
        assertThat(participations).hasSize(2);
    }

    @Test
    public void testFindParticipationByParticipantIdAndCharityId() {
        User user = this.userRepository.findByUsername("gIvanov");
        Charity charity = this.charityRepository.findByName("charity1");

        Participation participations = this.participationRepository
                .findParticipationByParticipantIdAndCharityId(user.getUser_id(), charity.getCharity_id());
        assertThat(participations).isNotNull();
    }

    @Test
    public void testGetNumberOfParticipantsInCharityWithOneParticipant() {
        Charity charity = this.charityRepository.findByName("charity1");

        Integer participants = this.participationRepository
                .getNumberOfParticipantsInCharity(charity.getCharity_id());
        assertThat(participants).isEqualTo(1);
    }

    @Test
    public void testGetNumberOfParticipantsInCharityWithOneParticipantHavingManyCharities() {
        Charity charity = this.charityRepository.findByName("charity1");
        User user1 = this.userRepository.findByUsername("gIvanov1");
        Charity charity1 = this.charityRepository.findByName("charity2");
        Participation newParticipation1 = new Participation(user1, charity1, null);

        this.participationRepository.save(newParticipation1);

        Integer participants = this.participationRepository
                .getNumberOfParticipantsInCharity(charity.getCharity_id());
        assertThat(participants).isEqualTo(1);
    }

    @Test
    public void testGetNumberOfParticipantsInCharityWithManyParticipants() {
        User user1 = this.userRepository.findByUsername("gIvanov1");
        Charity charity = this.charityRepository.findByName("charity1");
        Participation newParticipation1 = new Participation(user1, charity, null);

        this.participationRepository.save(newParticipation1);

        Integer participants = this.participationRepository
                .getNumberOfParticipantsInCharity(charity.getCharity_id());
        assertThat(participants).isEqualTo(2);
    }

    @Test
    public void testDeletingParitipantsFromCharityHavingOneParticipant() {
        Charity charity = this.charityRepository.findByName("charity1");

        this.participationRepository.deleteAllParticipationsByCharityID(charity.getCharity_id());

        List<Participation> participants = participationRepository.findAll();
        assertThat(participants).hasSize(0);
    }

    @Test
    public void testDeletingParitipantsFromCharityHavingManyParticipants() {
        User user1 = this.userRepository.findByUsername("gIvanov1");
        Charity charity = this.charityRepository.findByName("charity1");
        Participation newParticipation1 = new Participation(user1, charity, null);

        this.participationRepository.save(newParticipation1);
        this.participationRepository.deleteAllParticipationsByCharityID(charity.getCharity_id());

        List<Participation> participations = this.participationRepository.findAll();
        assertThat(participations).hasSize(0);
    }

    @Test
    public void testDeletingParitipantsFromCharityHavingManyParticipantsAndCharities() {
        User user1 = this.userRepository.findByUsername("gIvanov1");
        Charity charity = this.charityRepository.findByName("charity1");
        Charity charity2 = this.charityRepository.findByName("charity2");
        Participation newParticipation1 = new Participation(user1, charity2, null);

        this.participationRepository.save(newParticipation1);
        this.participationRepository.deleteAllParticipationsByCharityID(charity.getCharity_id());

        List<Participation> participations = this.participationRepository.findAll();
        assertThat(participations).hasSize(1);
    }


}
