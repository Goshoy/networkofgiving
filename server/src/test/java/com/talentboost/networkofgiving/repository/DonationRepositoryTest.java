package com.talentboost.networkofgiving.repository;

import com.talentboost.networkofgiving.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class DonationRepositoryTest {
    @Autowired
    private ICountryRepository countryRepository;
    @Autowired
    private ILocationRepository locationRepository;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private ICharityRepository charityRepository;
    @Autowired
    private IDonationRepository donationRepository;

    @BeforeEach
    public void setUp() {
        this.countryRepository.save(new Country("Bulgaria"));
        this.locationRepository.save(
                new Location(null, null, this.countryRepository.findByCountryName("Bulgaria")));

        Long country_id = this.countryRepository.findByCountryName("Bulgaria").getCountry_id();
        User user = new User("Georgi", "Ivanov", "gIvanov", 22,
                "Male", this.locationRepository.getLocationByStreeCityAndCountryID(null, null, country_id));
        this.userRepository.save(user);

        Charity charity = new Charity("charity1", "new charity", null, 5, 2000, user);
        this.charityRepository.save(charity);
    }

    @Test
    public void testAddedDonationIsActuallyAdded() {
        User user = this.userRepository.findByUsername("gIvanov");
        Charity charity = this.charityRepository.findByName("charity1");
        Donation newDonation = new Donation(50, user, charity, null);

        this.donationRepository.save(newDonation);

        List<Donation> donations = this.donationRepository.findAll();
        assertThat(donations).hasSize(1);
    }

    @Test
    public void testMultipleDonationsAreAdded() {
        User user = this.userRepository.findByUsername("gIvanov");
        Charity charity = this.charityRepository.findByName("charity1");
        Donation newDonation = new Donation(50, user, charity, null);
        Donation newDonation1 = new Donation(50, user, charity, null);

        this.donationRepository.save(newDonation);
        this.donationRepository.save(newDonation1);

        List<Donation> donations = this.donationRepository.findAll();
        assertThat(donations).hasSize(2);
    }

    @Test
    public void testRaisedAmountWithOneDonation() {
        final int donationAmount = 50;
        User user = this.userRepository.findByUsername("gIvanov");
        Charity charity = this.charityRepository.findByName("charity1");
        Donation donation = new Donation(donationAmount, user, charity, null);

        this.donationRepository.save(donation);

        Integer donations = this.donationRepository.getRaisedAmountForCharity(charity.getCharity_id());
        assertThat(donations).isEqualTo(donationAmount);
    }

    @Test
    public void testRaisedAmountWitManyDonations() {
        final int donationAmount = 50;
        User user = this.userRepository.findByUsername("gIvanov");
        Charity charity = this.charityRepository.findByName("charity1");
        Donation donation = new Donation(donationAmount, user, charity, null);
        Donation donation1 = new Donation(donationAmount, user, charity, null);
        Donation donation2 = new Donation(donationAmount, user, charity, null);

        this.donationRepository.save(donation);
        this.donationRepository.save(donation1);
        this.donationRepository.save(donation2);

        Integer donations = this.donationRepository.getRaisedAmountForCharity(charity.getCharity_id());
        assertThat(donations).isEqualTo(donationAmount * 3);
    }

    @Test
    public void testRaisedAmountWitManyDonationsHavingDifferentCharities() {
        final int donationAmount = 50;
        User user = this.userRepository.findByUsername("gIvanov");
        Charity charity = this.charityRepository.findByName("charity1");
        Charity charity2 = new Charity("charity2", "new charity", null, 3, 1000, user);
        this.charityRepository.save(charity2);
        Donation donation = new Donation(donationAmount, user, charity, null);
        Donation donation1 = new Donation(donationAmount, user, charity, null);
        Donation donation2 = new Donation(donationAmount, user, charity2, null);

        this.donationRepository.save(donation);
        this.donationRepository.save(donation1);
        this.donationRepository.save(donation2);

        Integer donations = this.donationRepository.getRaisedAmountForCharity(charity.getCharity_id());
        assertThat(donations).isEqualTo(donationAmount * 2);
    }

    @Test
    public void testDeletingSingleDonationToCharity() {
        final int donationAmount = 50;
        User user = this.userRepository.findByUsername("gIvanov");
        Charity charity = this.charityRepository.findByName("charity1");
        Donation donation = new Donation(donationAmount, user, charity, null);

        this.donationRepository.save(donation);
        this.donationRepository.deleteAllDonationsByCharityID(charity.getCharity_id());

        List<Donation> donations = this.donationRepository.findAll();
        assertThat(donations).isEmpty();
    }

    @Test
    public void testDeletingManyDonationsToCharity() {
        final int donationAmount = 50;
        User user = this.userRepository.findByUsername("gIvanov");
        Charity charity = this.charityRepository.findByName("charity1");
        Donation donation = new Donation(donationAmount, user, charity, null);
        Donation donation1 = new Donation(donationAmount, user, charity, null);

        this.donationRepository.save(donation);
        this.donationRepository.save(donation1);
        this.donationRepository.deleteAllDonationsByCharityID(charity.getCharity_id());

        List<Donation> donations = this.donationRepository.findAll();
        assertThat(donations).isEmpty();
    }

    @Test
    public void testDeletingManyDonationsToCharityHavingDifferentCharities() {
        final int donationAmount = 50;
        User user = this.userRepository.findByUsername("gIvanov");
        Charity charity = this.charityRepository.findByName("charity1");
        Charity charity2 = new Charity("charity2", "new charity", null, 3, 1000, user);
        this.charityRepository.save(charity2);
        Donation donation = new Donation(donationAmount, user, charity, null);
        Donation donation1 = new Donation(donationAmount, user, charity, null);
        Donation donation2 = new Donation(donationAmount, user, charity2, null);

        this.donationRepository.save(donation);
        this.donationRepository.save(donation1);
        this.donationRepository.save(donation2);
        this.donationRepository.deleteAllDonationsByCharityID(charity.getCharity_id());

        List<Donation> donations = this.donationRepository.findAll();
        assertThat(donations).hasSize(1);
    }
}
