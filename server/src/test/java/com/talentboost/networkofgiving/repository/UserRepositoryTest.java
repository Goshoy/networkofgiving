package com.talentboost.networkofgiving.repository;

import com.talentboost.networkofgiving.model.Country;
import com.talentboost.networkofgiving.model.Location;
import com.talentboost.networkofgiving.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class UserRepositoryTest {
    @Autowired
    private ICountryRepository countryRepository;
    @Autowired
    private ILocationRepository locationRepository;
    @Autowired
    private IUserRepository userRepository;

    @BeforeEach
    public void setUp() {
        this.countryRepository.save(new Country("Bulgaria"));
        this.locationRepository.save(
                new Location(null, null, this.countryRepository.findByCountryName("Bulgaria")));
    }

    @Test
    public void testAddedUserIsActuallyAdded() {
        Long country_id = this.countryRepository.findByCountryName("Bulgaria").getCountry_id();
        User newUser = new User("Georgi", "Ivanov", "gIvanov", 22,
                "Male", this.locationRepository.getLocationByStreeCityAndCountryID(null, null, country_id));

        this.userRepository.save(newUser);

        User user = this.userRepository.findByUsername("gIvanov");
        assertThat(user).isNotNull();
    }

    @Test
    public void testGettingUserByWrongUsername() {
        Long country_id = this.countryRepository.findByCountryName("Bulgaria").getCountry_id();
        User newUser = new User("Georgi", "Ivanov", "gIvanov", 22,
                "Male", this.locationRepository.getLocationByStreeCityAndCountryID(null, null, country_id));

        this.userRepository.save(newUser);

        User user = this.userRepository.findByUsername("gIvanv");
        assertThat(user).isNull();
    }

}
