package com.talentboost.networkofgiving.repository;

import com.talentboost.networkofgiving.model.Charity;
import com.talentboost.networkofgiving.model.Country;
import com.talentboost.networkofgiving.model.Location;
import com.talentboost.networkofgiving.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class CharityRepositoryTest {

    @Autowired
    private ICountryRepository countryRepository;
    @Autowired
    private ILocationRepository locationRepository;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private ICharityRepository charityRepository;

    @BeforeEach
    public void setUp() {
        this.countryRepository.save(new Country("Bulgaria"));
        this.locationRepository.save(
                new Location(null, null, this.countryRepository.findByCountryName("Bulgaria")));

        Long country_id = this.countryRepository.findByCountryName("Bulgaria").getCountry_id();
        User newUser = new User("Georgi", "Ivanov", "gIvanov", 22,
                "Male", this.locationRepository.getLocationByStreeCityAndCountryID(null, null, country_id));
        this.userRepository.save(newUser);
    }

    @Test
    public void testAddedCharityIsActuallyAdded() {
        User user = this.userRepository.findByUsername("gIvanov");
        Charity newCharity = new Charity("charity1", "New Chatiry", null,
                5, 1000, user);

        this.charityRepository.save(newCharity);

        Charity charity = this.charityRepository.findByName("charity1");
        assertThat(charity).isNotNull();
    }

    @Test
    public void testGettingCharityByWrongName() {
        User user = this.userRepository.findByUsername("gIvanov");
        Charity newCharity = new Charity("charity1", "New Chatiry", null,
                5, 1000, user);

        this.charityRepository.save(newCharity);

        Charity charity = this.charityRepository.findByName("charity");
        assertThat(charity).isNull();
    }

    @Test
    public void testSearchingCharitiesWithNoneMatching() {
        User user = this.userRepository.findByUsername("gIvanov");
        Charity charity = new Charity("charity1", "New Chatiry", null,
                5, 1000, user);

        this.charityRepository.save(charity);

        List<Charity> foundCharities = this.charityRepository.searchCharities("added");
        assertThat(foundCharities).isEmpty();
    }

    @Test
    public void testSearchingCharitiesWithTwoMatching() {
        User user = this.userRepository.findByUsername("gIvanov");
        Charity charity = new Charity("charity1", "New Chatiry", null,
                5, 1000, user);
        Charity charity1 = new Charity("charity2", "New Chatiry", null,
                5, 1000, user);
        Charity charity2 = new Charity("New", "New Chatiry", null,
                5, 1000, user);

        this.charityRepository.save(charity);
        this.charityRepository.save(charity1);
        this.charityRepository.save(charity2);

        List<Charity> foundCharities = this.charityRepository.searchCharities("charity");
        assertThat(foundCharities.size()).isEqualTo(2);
    }
}
