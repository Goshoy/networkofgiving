package com.talentboost.networkofgiving.repository;

import com.talentboost.networkofgiving.model.Country;
import com.talentboost.networkofgiving.model.Location;
import com.talentboost.networkofgiving.model.Login;
import com.talentboost.networkofgiving.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class LoginRepository {

    @Autowired
    private ICountryRepository countryRepository;
    @Autowired
    private ILocationRepository locationRepository;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private ILoginRepository loginRepository;

    @BeforeEach
    public void setUp() {
        this.countryRepository.save(new Country("Bulgaria"));
        this.locationRepository.save(
                new Location(null, null, this.countryRepository.findByCountryName("Bulgaria")));

        Long country_id = this.countryRepository.findByCountryName("Bulgaria").getCountry_id();
        User newUser = new User("Georgi", "Ivanov", "gIvanov", 22,
                "Male", this.locationRepository.getLocationByStreeCityAndCountryID(null, null, country_id));
        this.userRepository.save(newUser);
    }

    @Test
    public void testAddedLoginIsActuallyAdded() {
        User user = this.userRepository.findByUsername("gIvanov");
        Login newLogin = new Login("gIvanov", "1234", user);

        this.loginRepository.save(newLogin);

        Login login = this.loginRepository.findByUsername("gIvanov");
        assertThat(login).isNotNull();
    }

    @Test
    public void testGettingLoginWithWrongUsername() {
        User user = this.userRepository.findByUsername("gIvanov");
        Login newLogin = new Login("gIvanov", "1234", user);

        this.loginRepository.save(newLogin);

        Login login = this.loginRepository.findByUsername("gIvanovv");
        assertThat(login).isNull();
    }

    @Test
    public void testAuthenticatinWithRightCredentials() {
        User user = this.userRepository.findByUsername("gIvanov");
        Login newLogin = new Login("gIvanov", "1234", user);
        this.loginRepository.save(newLogin);

        Login login = this.loginRepository.authenticate("gIvanov", "1234");
        assertThat(login).isNotNull();
    }

    @Test
    public void testAuthenticatinWithWrongPassword() {
        User user = this.userRepository.findByUsername("gIvanov");
        Login newLogin = new Login("gIvanov", "1234", user);

        this.loginRepository.save(newLogin);

        Login login = this.loginRepository.authenticate("gIvanov", "12345");
        assertThat(login).isNull();
    }

    @Test
    public void testAuthenticatinWithWrongName() {
        User user = this.userRepository.findByUsername("gIvanov");
        Login newLogin = new Login("gIvanov", "1234", user);
        this.loginRepository.save(newLogin);

        Login login = this.loginRepository.authenticate("gIvanovv", "1234");
        assertThat(login).isNull();
    }

    @Test
    public void testAuthenticatinWithWrongNameAndPassword() {
        User user = this.userRepository.findByUsername("gIvanov");
        Login newLogin = new Login("gIvanov", "1234", user);

        this.loginRepository.save(newLogin);

        Login login = this.loginRepository.authenticate("gIvanovv", "12345");
        assertThat(login).isNull();
    }


}
