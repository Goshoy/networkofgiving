package com.talentboost.networkofgiving.service;

import com.talentboost.networkofgiving.model.Location;
import com.talentboost.networkofgiving.repository.ILocationRepository;
import com.talentboost.networkofgiving.service.interfaces.ILocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class LocationService implements ILocationService {

    @Autowired
    private ILocationRepository locationRepository;

    public Location getLocationByStreeCityAndCountryID(String street, String city, Long countryID) {
        return locationRepository.getLocationByStreeCityAndCountryID(street, city, countryID);
    }

    @Transactional
    public void addLocation(Location location) {
        locationRepository.save(location);
    }
}
