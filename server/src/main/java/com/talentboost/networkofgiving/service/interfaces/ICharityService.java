package com.talentboost.networkofgiving.service.interfaces;


import com.talentboost.networkofgiving.model.Charity;

import javax.transaction.Transactional;
import java.util.List;

public interface ICharityService {

    public Charity getCharityById(Long id);

    public List<Charity> getAllCharities();

    public List<Charity> searchCharities(String expr);

    @Transactional
    public Charity addCharity(String name, String description, String image, int numberOfParticipants,
                              int totalBudget, String ownerName);

    @Transactional
    public void deleteCharity(Long id);

    public Charity findByName(String name);

    @Transactional
    public void updateCharity(Long id, Integer numberOfParticipants, Integer totalBudget);
}
