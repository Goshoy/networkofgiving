package com.talentboost.networkofgiving.service.interfaces;


import com.talentboost.networkofgiving.model.Login;
import com.talentboost.networkofgiving.model.User;

import javax.transaction.Transactional;

public interface ILoginService {
    @Transactional
    public void addLogin(String username, String password, User user);

    public boolean authenticate(String username, String password);

    public Login findByUsername(String username);
}
