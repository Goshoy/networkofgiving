package com.talentboost.networkofgiving.service;

import com.talentboost.networkofgiving.model.Charity;
import com.talentboost.networkofgiving.model.User;
import com.talentboost.networkofgiving.repository.ICharityRepository;
import com.talentboost.networkofgiving.service.interfaces.ICharityService;
import com.talentboost.networkofgiving.service.interfaces.IDonationService;
import com.talentboost.networkofgiving.service.interfaces.IParticipationService;
import com.talentboost.networkofgiving.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class CharityService implements ICharityService {
    @Autowired
    private ICharityRepository charityRepository;
    @Autowired
    private IUserService userService;
    @Autowired
    private IDonationService donationService;
    @Autowired
    private IParticipationService participationService;

    public Charity getCharityById(Long id) {
        return this.getCharity(id);
    }

    public List<Charity> getAllCharities() {
        Iterable<Charity> it = this.charityRepository.findAll();

        return this.createList(it);
    }

    public List<Charity> searchCharities(String expr) {
        Iterable<Charity> it = this.charityRepository.searchCharities(expr);

        return this.createList(it);
    }

    @Transactional
    public Charity addCharity(String name, String description, String image, int numberOfParticipants,
                              int totalBudget, String ownerName) {
        if (this.findByName(name) != null) {
            throw new IllegalArgumentException("Charity name is taken!");
        }

        User owner = this.userService.findByUsername(ownerName);
        Charity charity = new Charity(name, description, image, numberOfParticipants, totalBudget, owner);

        return this.charityRepository.save(charity);
    }

    @Transactional
    public void deleteCharity(Long id) {
        Charity charity = this.getCharity(id);

        this.donationService.deleteAllDonationsByCharityID(charity.getCharity_id());
        this.participationService.deleteAllParticipationsByCharityID(charity.getCharity_id());
        // Here we can return the donated money back to the donators.

        this.charityRepository.delete(charity);
    }

    public Charity findByName(String name) {
        return this.charityRepository.findByName(name);
    }

    private List<Charity> createList(Iterable<Charity> it) {
        List<Charity> users = new ArrayList<>();
        it.forEach(users::add);
        return users;
    }

    @Transactional
    public void updateCharity(Long id, Integer numberOfParticipants, Integer totalBudget) {
        Charity currentCharity = this.getCharity(id);

        Integer currentParticipations = this.participationService.getNumberOfParticipantsInCharity(
                currentCharity.getCharity_id());
        if (currentParticipations > numberOfParticipants) {
            this.participationService.removeGivenNumberOfParticipants(
                    currentParticipations - numberOfParticipants, currentCharity.getCharity_id());
        }

        Integer currentDonations = this.donationService.getCollectedAmountForCharity(currentCharity.getCharity_id());
        if (currentDonations > totalBudget) {
            this.donationService.revertDonations(currentCharity.getCharity_id(), currentDonations - totalBudget);
        }

        this.charityRepository.update(id, numberOfParticipants, totalBudget);
    }

    private Charity getCharity(Long id) {
        Optional<Charity> optional = this.charityRepository.findById(id);
        if (optional.isEmpty()) {
            throw new NoSuchElementException("Charity does not exist.");
        }
        return optional.get();
    }
}
