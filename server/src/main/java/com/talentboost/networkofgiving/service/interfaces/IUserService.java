package com.talentboost.networkofgiving.service.interfaces;


import com.talentboost.networkofgiving.model.User;

import javax.transaction.Transactional;
import java.util.List;

public interface IUserService {

    public List<User> getAllUsers();

    @Transactional
    public User registerUser(String firstName, String lastName, String username, int age, String gender, String street, String cityName, String countryName);

    public User findByUsername(String username);

    public User getUser(String username);
}
