package com.talentboost.networkofgiving.service;

import com.talentboost.networkofgiving.model.Country;
import com.talentboost.networkofgiving.repository.ICountryRepository;
import com.talentboost.networkofgiving.service.interfaces.ICountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryService implements ICountryService {

    @Autowired
    private ICountryRepository countryRepository;

    public Country getCountryByName(String countryName) {
        return countryRepository.findByCountryName(countryName);
    }

    public List<Country> getAllCountries() {
        return this.countryRepository.findAll();
    }

}
