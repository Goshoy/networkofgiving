package com.talentboost.networkofgiving.service.interfaces;


import com.talentboost.networkofgiving.model.Participation;

import javax.transaction.Transactional;

public interface IParticipationService {
    @Transactional
    public Participation addParticipant(String participantName, String charityName);

    public Integer getNumberOfParticipantsInCharity(Long id);

    @Transactional
    public void deleteAllParticipationsByCharityID(Long charityID);

    public void removeGivenNumberOfParticipants(int number, Long charityId);
}
