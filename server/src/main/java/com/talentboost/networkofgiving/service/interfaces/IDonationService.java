package com.talentboost.networkofgiving.service.interfaces;


import com.talentboost.networkofgiving.model.Donation;

import javax.transaction.Transactional;
import java.util.List;

public interface IDonationService {

    @Transactional
    public Donation addDonation(int amount, String charityName, String username);

    public Integer getCollectedAmountForCharity(Long id);

    @Transactional
    public void deleteAllDonationsByCharityID(Long charityID);

    public void revertDonations(Long charityID, int amountExceeded);

    public List<Donation> getDonationsForCharityIdSortedByAmount(Long charityID);
}
