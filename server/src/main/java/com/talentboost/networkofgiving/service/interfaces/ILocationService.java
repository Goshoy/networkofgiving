package com.talentboost.networkofgiving.service.interfaces;


import com.talentboost.networkofgiving.model.Location;

import javax.transaction.Transactional;

public interface ILocationService {
    public Location getLocationByStreeCityAndCountryID(String street, String city, Long countryID);

    @Transactional
    public void addLocation(Location location);
}
