package com.talentboost.networkofgiving.service;

import com.talentboost.networkofgiving.model.Login;
import com.talentboost.networkofgiving.model.User;
import com.talentboost.networkofgiving.repository.ILoginRepository;
import com.talentboost.networkofgiving.service.interfaces.ILoginService;
import com.talentboost.networkofgiving.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class LoginService implements ILoginService {
    @Autowired
    private ILoginRepository loginRepository;
    @Autowired
    private IUserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public void addLogin(String username, String password, User user) {
        Login login = new Login(username, this.passwordEncoder.encode(password), user);
        this.loginRepository.save(login);
    }

    public boolean authenticate(String username, String password) {
        Login login = this.loginRepository.findByUsername(username);

        if (login == null) {
            throw new IllegalArgumentException("User does not exist.");
        }

        return this.passwordEncoder.matches(password, login.getPassword());
    }

    public Login findByUsername(String username) {
        return this.loginRepository.findByUsername(username);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
