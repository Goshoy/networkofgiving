package com.talentboost.networkofgiving.service;

import com.talentboost.networkofgiving.model.Charity;
import com.talentboost.networkofgiving.model.Participation;
import com.talentboost.networkofgiving.model.User;
import com.talentboost.networkofgiving.repository.IParticipationRepository;
import com.talentboost.networkofgiving.service.interfaces.ICharityService;
import com.talentboost.networkofgiving.service.interfaces.IParticipationService;
import com.talentboost.networkofgiving.service.interfaces.IUserService;
import com.talentboost.networkofgiving.util.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ParticipationService implements IParticipationService {
    @Autowired
    private IParticipationRepository participationRepository;
    @Autowired
    private IUserService userService;
    @Autowired
    private ICharityService charityService;

    @Transactional
    public Participation addParticipant(String participantName, String charityName) {
        User participant = userService.findByUsername(participantName);
        Charity charity = charityService.findByName(charityName);

        if (charity == null) {
            throw new IllegalArgumentException("Charity does not exist.");
        }

        if (participant == null) {
            throw new IllegalArgumentException("User does not exist.");
        }

        if (participationRepository.findParticipationByParticipantIdAndCharityId(participant.getUser_id(), charity.getCharity_id()) != null) {
            throw new IllegalArgumentException("Already a participant.");
        }

        Integer partipantsInCharity =
                participationRepository.getNumberOfParticipantsInCharity(charity.getCharity_id());
        if (partipantsInCharity == null) {
            partipantsInCharity = 0;
        }
        if (partipantsInCharity + 1 > charity.getNumberOfParticipants()) {
            throw new IllegalArgumentException("Already found participants.");
        }

        String currentTime = DateTimeUtil.getDateTimeAsString();
        Participation participation = new Participation(participant, charity, currentTime);

        return participationRepository.save(participation);
    }

    public Integer getNumberOfParticipantsInCharity(Long id) {
        Integer participations = this.participationRepository.getNumberOfParticipantsInCharity(id);
        return participations != null ? participations : 0;
    }

    @Transactional
    public void deleteAllParticipationsByCharityID(Long charityID) {
        this.participationRepository.deleteAllParticipationsByCharityID(charityID);
    }

    public void removeGivenNumberOfParticipants(int number, Long charityId) {
        this.participationRepository.removeGivenNumberOfParticipants(number, charityId);
    }
}
