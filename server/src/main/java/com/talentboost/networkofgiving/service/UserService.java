package com.talentboost.networkofgiving.service;

import com.talentboost.networkofgiving.model.Country;
import com.talentboost.networkofgiving.model.Location;
import com.talentboost.networkofgiving.model.User;
import com.talentboost.networkofgiving.repository.IUserRepository;
import com.talentboost.networkofgiving.service.interfaces.ICountryService;
import com.talentboost.networkofgiving.service.interfaces.ILocationService;
import com.talentboost.networkofgiving.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService implements IUserService {
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private ICountryService countryService;
    @Autowired
    private ILocationService locationService;

    public List<User> getAllUsers() {
        Iterable<User> it = this.userRepository.findAll();

        List<User> users = new ArrayList<User>();
        it.forEach(users::add);

        return users;
    }

    @Transactional
    public User registerUser(String firstName, String lastName, String username, int age, String gender, String street, String cityName, String countryName) {
        if (this.findByUsername(username) != null) {
            throw new IllegalArgumentException();
        }

        Country country = this.countryService.getCountryByName(countryName);
        Location location = this.locationService.getLocationByStreeCityAndCountryID(street, cityName, country.getCountry_id());

        if (location == null) {
            location = new Location(street, cityName, country);
            this.locationService.addLocation(location);
        }

        return this.userRepository.save(new User(firstName, lastName, username, age, gender, location));
    }

    public User findByUsername(String username) {
        return this.userRepository.findByUsername(username);
    }

    public User getUser(String username) {
        return this.userRepository.findByUsername(username);
    }
}