package com.talentboost.networkofgiving.service;

import com.talentboost.networkofgiving.model.Charity;
import com.talentboost.networkofgiving.model.Donation;
import com.talentboost.networkofgiving.model.User;
import com.talentboost.networkofgiving.repository.IDonationRepository;
import com.talentboost.networkofgiving.service.interfaces.ICharityService;
import com.talentboost.networkofgiving.service.interfaces.IDonationService;
import com.talentboost.networkofgiving.service.interfaces.IUserService;
import com.talentboost.networkofgiving.util.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class DonationService implements IDonationService {

    @Autowired
    private IDonationRepository donationRepository;
    @Autowired
    private IUserService userService;
    @Autowired
    private ICharityService charityService;

    @Transactional
    public Donation addDonation(int amount, String charityName, String username) {
        User user = userService.findByUsername(username);
        Charity charity = charityService.findByName(charityName);

        if (charity == null) {
            throw new IllegalArgumentException("Charity does not exist.");
        }

        if (user == null) {
            throw new IllegalArgumentException("User does not exist.");
        }

        Integer collectedDonations = donationRepository.getRaisedAmountForCharity(charity.getCharity_id());
        if (collectedDonations == null) {
            collectedDonations = 0;
        }

        if (charity.getTotalBudget() - collectedDonations < amount) {
            throw new IllegalArgumentException("The given amount exceeds the charity budget.");
        }

        String currentTime = DateTimeUtil.getDateTimeAsString();
        Donation donation = new Donation(amount, user, charity, currentTime);

        return donationRepository.save(donation);
    }

    public Integer getCollectedAmountForCharity(Long id) {
        Integer donations = this.donationRepository.getRaisedAmountForCharity(id);
        return donations != null ? donations : 0;
    }

    @Transactional
    public void deleteAllDonationsByCharityID(Long charityID) {
        this.donationRepository.deleteAllDonationsByCharityID(charityID);
    }

    public void revertDonations(Long charityID, int amountExceeded) {
        // Which donation ids to be deleted should be retrieved with sql statement, but i could not write it
        // so do it the hard way :(
        List<Long> donationIdsToDelete = new ArrayList<>();
        List<Donation> sortedDonations = this.getDonationsForCharityIdSortedByAmount(charityID);
        int donatedAmount = 0;
        int i = 0;
        while (donatedAmount < amountExceeded) {
            donationIdsToDelete.add(sortedDonations.get(i).getDonation_id());
            donatedAmount += sortedDonations.get(i++).getAmount();
        }

        // Here we can return the money back to the donators

        this.donationRepository.deleteDonationsWithIDs(donationIdsToDelete);
    }

    public List<Donation> getDonationsForCharityIdSortedByAmount(Long charityID) {
        return this.donationRepository.getDonationsForCharityIdSortedByAmount(charityID);
    }
}
