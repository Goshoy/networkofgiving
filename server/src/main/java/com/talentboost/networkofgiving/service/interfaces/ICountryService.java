package com.talentboost.networkofgiving.service.interfaces;


import com.talentboost.networkofgiving.model.Country;

import java.util.List;

public interface ICountryService {
    public Country getCountryByName(String countryName);

    public List<Country> getAllCountries();

}
