package com.talentboost.networkofgiving.controller.model;

public class ParticipationDTO {
    private String username;
    private String charity;

    public ParticipationDTO() {
    }

    public ParticipationDTO(String username, String charity) {
        this.username = username;
        this.charity = charity;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCharity() {
        return charity;
    }

    public void setCharity(String charity) {
        this.charity = charity;
    }
}
