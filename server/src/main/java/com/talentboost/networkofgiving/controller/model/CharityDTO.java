package com.talentboost.networkofgiving.controller.model;

public class CharityDTO {
    private Long id;
    private String name;
    private String description;
    private String image;
    private Integer numberOfParticipants;
    private Integer totalBudget;
    private String creator;

    public CharityDTO() {
    }

    public CharityDTO(Long id, String name, String description, String image, Integer numberOfParticipants, Integer totalBudget, String creator) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.numberOfParticipants = numberOfParticipants;
        this.totalBudget = totalBudget;
        this.creator = creator;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getNumberOfParticipants() {
        return numberOfParticipants;
    }

    public void setNumberOfParticipants(Integer numberOfParticipants) {
        this.numberOfParticipants = numberOfParticipants;
    }

    public Integer getTotalBudget() {
        return totalBudget;
    }

    public void setTotalBudget(Integer totalBudget) {
        this.totalBudget = totalBudget;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
