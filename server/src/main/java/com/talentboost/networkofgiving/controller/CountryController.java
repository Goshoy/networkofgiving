package com.talentboost.networkofgiving.controller;

import com.talentboost.networkofgiving.model.Country;
import com.talentboost.networkofgiving.service.interfaces.ICountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/countries", produces = MediaType.APPLICATION_JSON_VALUE)
public class CountryController {

    @Autowired
    private ICountryService countryService;

    @GetMapping(value = "/all")
    public List<Country> getAllCountries() {
        return this.countryService.getAllCountries();
    }
}
