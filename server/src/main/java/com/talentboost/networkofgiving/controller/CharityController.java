package com.talentboost.networkofgiving.controller;

import com.talentboost.networkofgiving.controller.model.CharityDTO;
import com.talentboost.networkofgiving.controller.model.CharityProgress;
import com.talentboost.networkofgiving.controller.model.DonationDTO;
import com.talentboost.networkofgiving.controller.model.ParticipationDTO;
import com.talentboost.networkofgiving.model.Charity;
import com.talentboost.networkofgiving.model.Donation;
import com.talentboost.networkofgiving.model.Participation;
import com.talentboost.networkofgiving.service.interfaces.ICharityService;
import com.talentboost.networkofgiving.service.interfaces.IDonationService;
import com.talentboost.networkofgiving.service.interfaces.IParticipationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/charities", produces = MediaType.APPLICATION_JSON_VALUE)
public class CharityController {
    @Autowired
    private ICharityService charityService;
    @Autowired
    private IDonationService donationService;
    @Autowired
    private IParticipationService participationService;

    @GetMapping("/all")
    public List<CharityDTO> getAllCharities() {
        return this.charityService.getAllCharities().stream().map(this::mapCharity).collect(Collectors.toList());
    }

    @GetMapping(value = "/search/{searchString}")
    public List<CharityDTO> searchCharities(@PathVariable("searchString") String searchString) {
        return this.charityService.searchCharities(searchString).stream().map(this::mapCharity).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public CharityDTO getCharity(@PathVariable("id") Long id) {
        return mapCharity(this.charityService.getCharityById(id));
    }

    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public CharityDTO addCharity(@RequestBody @Valid CharityDTO charity) {
        try {
            return mapCharity(this.charityService.addCharity(charity.getName(), charity.getDescription(), charity.getImage(),
                    charity.getNumberOfParticipants(), charity.getTotalBudget(), charity.getCreator()));
        } catch (IllegalArgumentException exc) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, exc.getMessage(), exc);
        }
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCharity(@PathVariable("id") Long id) {
        this.charityService.deleteCharity(id);
    }

    @GetMapping("/progress/{id}")
    public CharityProgress getCharityProgress(@PathVariable("id") Long id) {
        Integer donations = this.donationService.getCollectedAmountForCharity(id);
        Integer participations = this.participationService.getNumberOfParticipantsInCharity(id);

        return new CharityProgress(donations, participations);
    }

    @PostMapping(value = "/donate", consumes = MediaType.APPLICATION_JSON_VALUE)
    public DonationDTO donateToCharity(@RequestBody @Valid DonationDTO donation) {
        try {
            return mapDonation(this.donationService.addDonation(donation.getDonation(), donation.getCharity(), donation.getUsername()));
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, e.getMessage(), e);
        }
    }

    @PostMapping(value = "/participate", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ParticipationDTO participateInCharity(@RequestBody @Valid ParticipationDTO participation) {
        try {
            return mapParticipation(this.participationService.addParticipant(participation.getUsername(), participation.getCharity()));
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, e.getMessage(), e);
        }
    }

    @PatchMapping(value = "/update/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ParticipationDTO updateCharity(@PathVariable("id") Long id, @RequestBody @Valid CharityDTO charity) {
        this.charityService.updateCharity(id, charity.getNumberOfParticipants(), charity.getTotalBudget());
        System.out.println(charity);
        return null;
    }

    public ParticipationDTO mapParticipation(Participation participation) {
        return new ParticipationDTO(participation.getParticipant().getUsername(), participation.getCharity().getName());
    }

    public DonationDTO mapDonation(Donation donation) {
        return new DonationDTO(donation.getAmount(), donation.getDonator().getUsername(), donation.getCharity().getName());
    }

    public CharityDTO mapCharity(Charity charity) {
        return new CharityDTO(charity.getCharity_id(), charity.getName(),
                charity.getDescription(), charity.getImage(), charity.getNumberOfParticipants(), charity.getTotalBudget(),
                charity.getOwner().getUsername());
    }

}


