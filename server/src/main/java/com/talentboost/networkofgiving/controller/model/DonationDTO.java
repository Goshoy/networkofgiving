package com.talentboost.networkofgiving.controller.model;

public class DonationDTO {
    private Integer donation;
    private String username;
    private String charity;

    public DonationDTO() {
    }

    public DonationDTO(Integer donation, String username, String charity) {
        this.donation = donation;
        this.username = username;
        this.charity = charity;
    }

    public Integer getDonation() {
        return donation;
    }

    public void setDonation(Integer donation) {
        this.donation = donation;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCharity() {
        return charity;
    }

    public void setCharity(String charity) {
        this.charity = charity;
    }
}
