package com.talentboost.networkofgiving.controller;

import com.talentboost.networkofgiving.controller.model.UserDTO;
import com.talentboost.networkofgiving.model.User;
import com.talentboost.networkofgiving.service.interfaces.ILoginService;
import com.talentboost.networkofgiving.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {
    @Autowired
    private IUserService userService;
    @Autowired
    private ILoginService loginService;

    @GetMapping
    public List<User> getAllUsers() {
        return this.userService.getAllUsers();
    }

    @GetMapping(value = "/{username}")
    public UserDTO getUser(@PathVariable("username") String username) {
        return mapUser(this.userService.getUser(username));
    }

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO registerUser(@RequestBody @Valid UserDTO userDTO) {
        try {
            System.out.println(userDTO);
            User registeredUser = this.userService.registerUser(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getUsername(),
                    userDTO.getAge(), userDTO.getGender(), null, null, userDTO.getCountry());
            this.loginService.addLogin(userDTO.getUsername(), userDTO.getPassword(), registeredUser);

            return mapUser(registeredUser);
        } catch (IllegalArgumentException exc) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Username is taken!", exc);
        }
    }

    public UserDTO mapUser(User user) {
        return new UserDTO(user.getFirstName(), user.getLastName(), user.getUsername(), user.getAge(),
                user.getGender(), user.getLocation().getCountry().getCountryName(), null);
    }
}
