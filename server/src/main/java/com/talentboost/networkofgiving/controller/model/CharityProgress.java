package com.talentboost.networkofgiving.controller.model;

public class CharityProgress {
    private Integer donations;
    private Integer participants;

    public CharityProgress() {
    }

    public CharityProgress(Integer donations, Integer participants) {
        this.donations = donations;
        this.participants = participants;
    }

    public Integer getDonations() {
        return donations;
    }

    public void setDonations(Integer donations) {
        this.donations = donations;
    }

    public Integer getParticipants() {
        return participants;
    }

    public void setParticipants(Integer participants) {
        this.participants = participants;
    }
}
