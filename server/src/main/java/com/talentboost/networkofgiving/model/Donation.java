package com.talentboost.networkofgiving.model;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table(name = "donations")
public class Donation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long donation_id;
    @NotNull
    int amount;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "donator_id")
    private User donator;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "charity_id")
    private Charity charity;
    private String date;

    public Donation() {
    }

    public Donation(int amount, User donator, Charity charity, String date) {
        this.amount = amount;
        this.donator = donator;
        this.charity = charity;
        this.date = date;
    }

    public Long getDonation_id() {
        return donation_id;
    }

    public void setDonation_id(Long donation_id) {
        this.donation_id = donation_id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public User getDonator() {
        return donator;
    }

    public void setDonator(User donator) {
        this.donator = donator;
    }

    public Charity getCharity() {
        return charity;
    }

    public void setCharity(Charity charity) {
        this.charity = charity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
