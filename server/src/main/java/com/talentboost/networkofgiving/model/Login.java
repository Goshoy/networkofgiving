package com.talentboost.networkofgiving.model;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table(name = "logins")
public class Login {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long login_id;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Login() {
    }

    public Login(String username, String password, User user) {
        this.username = username;
        this.password = password;
        this.user = user;
    }

    public Long getLogin_id() {
        return login_id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "Login{" +
                "login_id=" + login_id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", user=" + user +
                '}';
    }
}
