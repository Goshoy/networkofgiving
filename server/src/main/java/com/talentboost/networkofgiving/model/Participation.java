package com.talentboost.networkofgiving.model;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table(name = "participations")
public class Participation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long participation_id;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "participant_id")
    private User participant;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "charity_id")
    private Charity charity;
    private String date;

    public Participation() {
    }

    public Participation(User donator, Charity charity, String date) {
        this.participant = donator;
        this.charity = charity;
        this.date = date;
    }

    public Long getParticipation_id() {
        return participation_id;
    }

    public void setParticipation_id(Long participation_id) {
        this.participation_id = participation_id;
    }

    public User getParticipant() {
        return participant;
    }

    public void setParticipant(User participant) {
        this.participant = participant;
    }

    public Charity getCharity() {
        return charity;
    }

    public void setCharity(Charity charity) {
        this.charity = charity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
