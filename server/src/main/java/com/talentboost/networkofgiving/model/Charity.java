package com.talentboost.networkofgiving.model;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table(name = "charities")
public class Charity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long charity_id;
    @NotNull
    @Column(unique = true)
    String name;
    @NotNull
    String description;
    String image;
    int numberOfParticipants;
    int totalBudget;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;

    public Charity() {
    }

    public Charity(String name, String description, String image, int numberOfParticipants, int totalBudget, User owner) {
        this.name = name;
        this.description = description;
        this.image = image;
        this.numberOfParticipants = numberOfParticipants;
        this.totalBudget = totalBudget;
        this.owner = owner;
    }

    public Long getCharity_id() {
        return charity_id;
    }

    public void setCharity_id(Long charity_id) {
        this.charity_id = charity_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getNumberOfParticipants() {
        return numberOfParticipants;
    }

    public void setNumberOfParticipants(int numberOfParticipants) {
        this.numberOfParticipants = numberOfParticipants;
    }

    public int getTotalBudget() {
        return totalBudget;
    }

    public void setTotalBudget(int totalBudget) {
        this.totalBudget = totalBudget;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
