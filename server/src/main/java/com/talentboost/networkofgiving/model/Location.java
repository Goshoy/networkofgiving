package com.talentboost.networkofgiving.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "locations")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long location_id;
    private String streetAddress;
    private String city;
    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    public Location() {
    }

    public Location(String streetAddress, String city, Country country) {
        this.streetAddress = streetAddress;
        this.city = city;
        this.country = country;
    }

    public Long getLocation_id() {
        return location_id;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public String getCity() {
        return city;
    }

    public Country getCountry() {
        return country;
    }

    @Override
    public String toString() {
        return "Location{" +
                "streetAddress='" + streetAddress + '\'' +
                ", city='" + city + '\'' +
                ", country=" + country +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Objects.equals(location_id, location.location_id) &&
                Objects.equals(streetAddress, location.streetAddress) &&
                Objects.equals(city, location.city) &&
                Objects.equals(country, location.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(location_id, streetAddress, city, country);
    }
}
