package com.talentboost.networkofgiving.repository;

import com.talentboost.networkofgiving.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICountryRepository extends JpaRepository<Country, Long> {
    Country findByCountryName(final String countryName);
}
