package com.talentboost.networkofgiving.repository;

import com.talentboost.networkofgiving.model.Participation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface IParticipationRepository extends JpaRepository<Participation, Long> {
    @Query(value = "select * from participations where participant_id = ?1 and charity_id = ?2", nativeQuery = true)
    Participation findParticipationByParticipantIdAndCharityId(Long participantID, Long charityID);

    @Query(value = "select count(*) from participations where charity_id = ?1 group by charity_id", nativeQuery = true)
    Integer getNumberOfParticipantsInCharity(Long participantID);

    @Modifying
    @Query(value = "delete from participations where charity_id = ?1", nativeQuery = true)
    void deleteAllParticipationsByCharityID(Long charityID);

    @Modifying
    @Query(value = "delete from participations where charity_id = ?2 order by date asc limit ?1", nativeQuery = true)
    void removeGivenNumberOfParticipants(int number, Long charityId);
}
