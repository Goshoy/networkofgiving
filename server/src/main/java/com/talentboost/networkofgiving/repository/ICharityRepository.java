package com.talentboost.networkofgiving.repository;

import com.talentboost.networkofgiving.model.Charity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ICharityRepository extends JpaRepository<Charity, Long> {
    Charity findByName(String name);

    @Query(value = "select * from charities where name like %?1%", nativeQuery = true)
    List<Charity> searchCharities(String expr);

    @Modifying
    @Query(value = "update charities set number_of_participants = ?2, total_budget = ?3 where charity_id = ?1", nativeQuery = true)
    void update(Long id, Integer numberOfParticipants, Integer totalBudget);
}
