package com.talentboost.networkofgiving.repository;

import com.talentboost.networkofgiving.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ILocationRepository extends JpaRepository<Location, Long> {

    @Query(value = "select * from locations where (street_address = ?1 or street_address = ?1)" +
            " and " +
            " city =?2 and country_id =?3", nativeQuery = true)
    Location getLocationByStreeCityAndCountryID(String street, String city, Long countryID);
}
