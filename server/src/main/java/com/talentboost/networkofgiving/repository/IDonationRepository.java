package com.talentboost.networkofgiving.repository;

import com.talentboost.networkofgiving.model.Donation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IDonationRepository extends JpaRepository<Donation, Long> {
    @Query(value = "select sum(amount) from donations where charity_id = ?1 group by charity_id",
            nativeQuery = true)
    Integer getRaisedAmountForCharity(Long charityId);

    @Modifying
    @Query(value = "delete from donations where charity_id= ?1", nativeQuery = true)
    void deleteAllDonationsByCharityID(Long charityID);

    @Query(value = "select * from donations where charity_id = ?1 order by amount",
            nativeQuery = true)
    List<Donation> getDonationsForCharityIdSortedByAmount(Long charityId);

    @Modifying
    @Query(value = "delete from donations where donation_id in ?1", nativeQuery = true)
    void deleteDonationsWithIDs(List<Long> donationIdsToDelete);
}
