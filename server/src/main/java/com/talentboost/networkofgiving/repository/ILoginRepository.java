package com.talentboost.networkofgiving.repository;

import com.talentboost.networkofgiving.model.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ILoginRepository extends JpaRepository<Login, Long> {
    Login findByUsername(String username);

    @Query(value = "select * from logins where username = ?1 and password = ?2", nativeQuery = true)
    Login authenticate(String username, String password);
}
