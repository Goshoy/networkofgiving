package com.talentboost.networkofgiving.security;

import com.talentboost.networkofgiving.model.Login;
import com.talentboost.networkofgiving.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UserPrincipalDetailsService implements UserDetailsService {

    @Autowired
    private LoginService loginService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Login login = loginService.findByUsername(s);
        if (login == null) {
            // Should not be here!!
            throw new ResponseStatusException(
                    HttpStatus.FORBIDDEN, "Bad credentials", new IllegalArgumentException());
        }
        return new UserPrincipal(login);
    }
}
