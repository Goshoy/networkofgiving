# Network Of Giving

## Description
This is my final project for VMware`s Talent Boost Academy. Website for charities with primary operations
like register, login, create charity, donate, participate, edit charity, search and profile page.
The stack used to implement the project is Spring Boot with Angular 8.
The data is stored in mysql database which is configured locally.
JPA is used for access to the database. The website has basic authentication implemented using jwt tokens.  